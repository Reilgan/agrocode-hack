from starlette import status

from app.core import ValidationError, ValidationErrorMixin


@ValidationError.register
class RegistrationException(ValidationErrorMixin, Exception):
    def __init__(self, **kwargs):
        super().__init__(code=self.code, message=self.message, **kwargs)


class EmailIsNotExist(RegistrationException):
    code = 'registration'
    message = "Mail with this key does not exist"


class InvalidVerificationCode(RegistrationException):
    code = 'registration'
    message = "Invalid verification code"
