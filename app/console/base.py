from typing import Optional
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from app.settings import DATABASE_URL
from app.core.database.db import DataBase


class ConsoleCommand:

    def __init__(self):
        self.db: Optional[DataBase] = None
        self.session: Optional[Session] = None

    def __call__(self, *args, **kwargs):
        self._configure()
        self._execute()

    def _configure(self):
        self.db = DataBase(create_engine(DATABASE_URL))

    def _execute(self):
        with self.db.session() as session:
            self.session = session
            self.execute()

    def execute(self):
        raise NotImplementedError

