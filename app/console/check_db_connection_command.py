from app.console.base import ConsoleCommand


class CheckDBConnectionCommand(ConsoleCommand):
    def execute(self):
        try:
            self.session.begin()
            print('success')
        except Exception:
            print('fail')
        finally:
            self.session.close()
