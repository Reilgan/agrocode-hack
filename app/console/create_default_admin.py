from app.console.base import ConsoleCommand
from app.business.user import User
from app.schemas import CreateUserSchema


class CreateDefaultAdminCommand(ConsoleCommand):
    def execute(self):
        try:
            User.create_in_db(
                CreateUserSchema(
                        email="luginich.danil@gmail.com",
                        username="Reilgan",
                        phone_number="+79080578569",
                        password="123456789",
                        role="admin",
                        is_active=True
                )
            )
            print('success')
        except Exception:
            print('fail')



