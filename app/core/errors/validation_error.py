from __future__ import annotations
from typing import Optional, Set, Type, Tuple, Dict, Any, List
from pydantic.errors import PydanticErrorMixin
from pydantic import ValidationError as PydanticValidationError
from fastapi.exceptions import RequestValidationError
from app.utils import classproperty, to_camel_case


class ValidationErrorMixin(PydanticErrorMixin):
    field: Optional[str]
    errors: Optional[List[ValidationErrorMixin]] = None
    message: str

    def __init__(self, field: Optional[str] = None, **kwargs):
        super().__init__(**kwargs)
        self.field = to_camel_case(field)

    def __str__(self):
        message = getattr(self, 'message', None)
        if message is not None:
            return message
        return super().__str__()


class ValidationError(Exception):

    _registered: Set[Type[ValidationErrorMixin]] = {
        RequestValidationError,
    }

    @classproperty
    def registered(cls) -> Tuple[Type[ValidationErrorMixin], ...]:
        return tuple([*cls._registered, ValidationError])

    def __init__(self, message: str = None, code: str = None, field: str = None):
        self.message = message
        self.code = code
        self.field = field

    @classmethod
    def register(cls, exc_cls: Type[ValidationErrorMixin]) -> Type[ValidationErrorMixin]:
        if exc_cls not in cls._registered:
            cls._registered.add(exc_cls)
        return exc_cls

    @staticmethod
    def update_errors(errors: Dict, ex: ValidationErrorMixin) -> None:
        field = getattr(ex, 'field', None)
        if not field:
            errors['root'].append({
                'code': ex.code,
                'msg': str(ex)
            })
        else:
            errors['fields'].setdefault(field, []).append({
                'code': ex.code,
                'msg': str(ex)
            })

    @classmethod
    def from_exc(cls, exc: ValidationErrorMixin) -> Dict[str, Any]:
        if isinstance(exc, PydanticValidationError):
            return cls.from_pydantic_exc(exc)
        else:
            errors = {
                'fields': {},
                'root': [],
            }
            if hasattr(exc, 'errors') and exc.errors:
                for ex in exc.errors:
                    cls.update_errors(errors, ex)
            else:
                cls.update_errors(errors, exc)
        return errors

    @classmethod
    def from_pydantic_exc(cls, exc: PydanticValidationError) -> Dict[str, Any]:
        errors = {
            'fields': {},
            'root': [],
        }
        for error in exc.errors():
            try:
                code = '.'.join(error.get('type', '').split('.')[1:])
            except Exception:
                code = None
            field = error.get('loc', (None, None))[1]
            if not field or field == '__root__':
                errors['root'].append({
                    'code': code,
                    'msg': error.get('msg')
                })
            else:
                errors['fields'].setdefault(field, []).append({
                    'code': code,
                    'msg': error.get('msg')
                })
        return errors
