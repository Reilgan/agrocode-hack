from fastapi import APIRouter

from app.endpoints.auth_endpoint import auth_route
from app.endpoints.user_endpoint import user_router
from app.endpoints.division_endpoint import division_router
from app.endpoints.work_area_endpoint import work_area_router
from app.endpoints.working_tools_endpoint import working_tools_router
from app.endpoints.task_endpoint import task_router
from app.endpoints.working_tools_categories_endpoint import working_tools_categories_router
from app.endpoints.task_parameters_endpoint import task_parameters_router

router = APIRouter()
router.include_router(auth_route)
router.include_router(user_router)
router.include_router(division_router)
router.include_router(work_area_router)
router.include_router(working_tools_router)
router.include_router(task_router)
router.include_router(working_tools_categories_router)
router.include_router(task_parameters_router)
