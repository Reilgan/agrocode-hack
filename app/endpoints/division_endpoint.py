from fastapi import APIRouter, Depends
from starlette import status

from app.core.auth import auth, RoleChecker
from app.models import UserModel, DivisionModel, WorkAreaModel, WorkingToolsModel
from app.core.database.db import data_base
from app.schemas.division_schema import DivisionSchema, DivisionOutSchema, DivisionOutListSchema
from app.utils import object_as_dict, clean_update_data
from sqlalchemy import delete, select, update

division_router = APIRouter(prefix='/division', tags=['Division'])
allow_cud_resource = RoleChecker([UserModel.RoleEnum.admin])
add_remove_user_in_division = RoleChecker([UserModel.RoleEnum.admin])
add_remove_work_area_in_division = RoleChecker([UserModel.RoleEnum.admin])
add_remove_working_tools_in_division = RoleChecker([UserModel.RoleEnum.admin])


@division_router.post(
    '/create',
    summary='create division',
    status_code=status.HTTP_200_OK,
    response_model=DivisionOutSchema,
    dependencies=[Depends(allow_cud_resource)]
)
async def create(data: DivisionSchema):
    with data_base.session() as session:
        division = DivisionModel(**data.model_dump())
        session.add_all([division])
        session.flush()
        return DivisionOutSchema(
            id=division.id,
            name=division.name,
            users=[object_as_dict(x) for x in division.users],
            work_area=[object_as_dict(x) for x in division.work_area],
            working_tools=[object_as_dict(x) for x in division.working_tools]
        )


@division_router.put(
    '/update/{id}',
    summary='create division',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_cud_resource)]
)
async def update_division(id, data: DivisionSchema):
    with data_base.session() as session:
        update_data = clean_update_data(data.model_dump())
        if not update_data:
            return
        session.execute(
            update(DivisionModel).
            where(DivisionModel.id == id).
            values(**update_data)
        )


@division_router.delete(
    '/{id}',
    summary='delete division',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_cud_resource)]
)
async def delete_division(id):
    with data_base.session() as session:
        session.execute(delete(DivisionModel).where(DivisionModel.id == id))


@division_router.get(
    '/{id}',
    summary='get division',
    status_code=status.HTTP_200_OK,
    response_model=DivisionOutSchema,
    dependencies=[Depends(auth)]
)
async def get(id):
    with data_base.session() as session:
        query = select(DivisionModel).filter_by(id=id)
        division = session.scalars(query).one()
        return DivisionOutSchema(
            id=division.id,
            name=division.name,
            users=[object_as_dict(x) for x in division.users]
        )


@division_router.get(
    '/list/',
    summary='get division list',
    status_code=status.HTTP_200_OK,
    response_model=DivisionOutListSchema,
    dependencies=[Depends(auth)]
)
async def list():
    with data_base.session() as session:
        query = select(DivisionModel)
        divisions = session.scalars(query).all()
        result = []
        for division in divisions:
            d = object_as_dict(division)
            d['users'] = []
            for user in division.users:
                user_dict = object_as_dict(user)
                user_dict['profile'] = object_as_dict(user.profile)
                d['users'].append(user_dict)
            result.append(d)
        return DivisionOutListSchema(
            divisions=result
        )


@division_router.post(
    '/{id}/add-user/{user_id}',
    summary='add user in division',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(add_remove_user_in_division)]
)
async def add_user(id, user_id):
    with data_base.session() as session:
        query = select(DivisionModel).filter_by(id=id)
        division = session.scalars(query).one()
        query = select(UserModel).filter_by(id=user_id)
        division.users.append(session.scalars(query).one())
        session.flush()


@division_router.post(
    '/{id}/remove-user/{user_id}',
    summary='remove user from division',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(add_remove_user_in_division)]
)
async def remove_user(id, user_id):
    with data_base.session() as session:
        query = select(DivisionModel).filter_by(id=id)
        division = session.scalars(query).one()
        query = select(UserModel).filter_by(id=user_id)
        division.users.remove(session.scalars(query).one())
        session.flush()


@division_router.post(
    '/{id}/add-work-area/{work_area_id}',
    summary='add work_area in division',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(add_remove_work_area_in_division)]
)
async def add_work_area(id, work_area_id):
    with data_base.session() as session:
        query = select(DivisionModel).filter_by(id=id)
        division = session.scalars(query).one()
        query = select(WorkAreaModel).filter_by(id=work_area_id)
        division.work_area.append(session.scalars(query).one())
        session.flush()


@division_router.post(
    '/{id}/remove-work-area/{work_area_id}',
    summary='remove work_area from division',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(add_remove_work_area_in_division)]
)
async def remove_work_area(id, work_area_id):
    with data_base.session() as session:
        query = select(DivisionModel).filter_by(id=id)
        division = session.scalars(query).one()
        query = select(WorkAreaModel).filter_by(id=work_area_id)
        division.work_area.remove(session.scalars(query).one())
        session.flush()


@division_router.post(
    '/{id}/add-working-tools/{working_tools_id}',
    summary='add working_tools in division',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(add_remove_working_tools_in_division)]
)
async def add_working_tools(id, working_tools_id):
    with data_base.session() as session:
        query = select(DivisionModel).filter_by(id=id)
        division = session.scalars(query).one()
        query = select(WorkingToolsModel).filter_by(id=working_tools_id)
        division.working_tools.append(session.scalars(query).one())
        session.flush()


@division_router.post(
    '/{id}/remove-working-tools/{working_tools_id}',
    summary='remove working_tools from division',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(add_remove_working_tools_in_division)]
)
async def remove_working_tools(id, working_tools_id):
    with data_base.session() as session:
        query = select(DivisionModel).filter_by(id=id)
        division = session.scalars(query).one()
        query = select(WorkingToolsModel).filter_by(id=working_tools_id)
        division.working_tools.remove(session.scalars(query).one())
        session.flush()
