from fastapi import APIRouter, Depends
from starlette import status
from app.core.auth import RoleChecker
from app.models import UserModel, TaskModel, WorkingToolsModel, WorkAreaModel, TaskParametersModel
from app.schemas.task_schema import TaskOutSchema, TaskOutListSchema, TaskSchema
from app.core.database.db import data_base
from app.utils import object_as_dict, clean_update_data
from sqlalchemy import delete, select, update

task_router = APIRouter(prefix='/task', tags=['Task'])
allow_crud_resource = RoleChecker([UserModel.RoleEnum.agronomist, UserModel.RoleEnum.admin])
allow_append_remove_working_tools = RoleChecker([UserModel.RoleEnum.agronomist, UserModel.RoleEnum.admin])


@task_router.post(
    '/create',
    summary='create talk',
    status_code=status.HTTP_200_OK,
    response_model=TaskOutSchema,
)
async def create(data: TaskSchema, user: UserModel = Depends(allow_crud_resource)):
    with data_base.session() as session:
        instance = TaskModel(**data.model_dump(), owner_id=user.id)
        session.add_all([instance])
        session.flush()
        return TaskOutSchema(**object_as_dict(instance))


@task_router.put(
    '/update/{id}',
    summary='create task',
    status_code=status.HTTP_200_OK,
)
async def update_task(id, data: TaskSchema, user: UserModel = Depends(allow_crud_resource)):
    with data_base.session() as session:
        update_data = clean_update_data(data.model_dump())
        if user.role == UserModel.RoleEnum.machine_operator:
            update_data = update_data.pop('status', None)

        if not update_data:
            return
        session.execute(
            update(TaskModel).
            where(TaskModel.id == id).
            values(**update_data)
        )


@task_router.delete(
    '/{id}',
    summary='delete task',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_crud_resource)]
)
async def delete_task(id):
    with data_base.session() as session:
        session.execute(delete(TaskModel).where(TaskModel.id == id))


@task_router.get(
    '/{id}',
    summary='get task by id',
    status_code=status.HTTP_200_OK,
    response_model=TaskOutSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def get(id):
    with data_base.session() as session:
        query = select(TaskModel).filter_by(id=id)
        instance = session.scalars(query).one()
        return TaskOutSchema(**object_as_dict(instance))


@task_router.get(
    '/list/',
    summary='get work tool list',
    status_code=status.HTTP_200_OK,
    response_model=TaskOutListSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def list():
    with data_base.session() as session:
        query = select(TaskModel)
        instance = session.scalars(query).all()
        result = []
        for task in instance:
            data = object_as_dict(task)
            data['working_tools'] = [x.id for x in task.working_tools]
            data['task_parameters'] = [x.id for x in task.task_parameters]
            result.append(data)

        return TaskOutListSchema(
            tasks=result
        )


@task_router.post(
    "/{id}/add-working-tools/{working_tools_id}",
    summary='add working tool for task',
    dependencies=[Depends(allow_append_remove_working_tools)]
)
async def add_working_tools(id, working_tools_id):
    with data_base.session() as session:
        query = select(TaskModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(WorkingToolsModel).filter_by(id=working_tools_id)
        instance.working_tools.append(session.scalars(query).one())
        session.flush()


@task_router.post(
    "/{id}/remove-working-tools/{working_tools_id}",
    summary='remove working tool for task',
    dependencies=[Depends(allow_append_remove_working_tools)]
)
async def remove_working_tools(id, working_tools_id):
    with data_base.session() as session:
        query = select(TaskModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(WorkingToolsModel).filter_by(id=working_tools_id)
        instance.working_tools.remove(session.scalars(query).one())
        session.flush()


@task_router.post(
    "/{id}/add-work-area/{work_area_id}",
    summary='add work area for task',
    dependencies=[Depends(allow_append_remove_working_tools)]
)
async def add_work_area(id, work_area_id):
    with data_base.session() as session:
        query = select(TaskModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(WorkAreaModel).filter_by(id=work_area_id)
        instance.work_area = session.scalars(query).one()
        session.flush()


@task_router.post(
    "/{id}/remove-work-area/",
    summary='remove work area for task',
    dependencies=[Depends(allow_append_remove_working_tools)]
)
async def remove_work_area(id):
    with data_base.session() as session:
        query = select(TaskModel).filter_by(id=id)
        instance = session.scalars(query).one()
        instance.work_area = None
        session.flush()


@task_router.post(
    "/{id}/add-task-parameter/{task_parameter_id}",
    summary='add task parameter for task',
    dependencies=[Depends(allow_append_remove_working_tools)]
)
async def add_task_parameter(id, task_parameter_id):
    with data_base.session() as session:
        query = select(TaskModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(TaskParametersModel).filter_by(id=task_parameter_id)
        instance.task_parameters.append(session.scalars(query).one())
        session.flush()


@task_router.post(
    "/{id}/remove-task-parameter/{task_parameter_id}",
    summary='remove work area for task',
    dependencies=[Depends(allow_append_remove_working_tools)]
)
async def remove_work_area(id, task_parameter_id):
    with data_base.session() as session:
        query = select(TaskModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(TaskParametersModel).filter_by(id=task_parameter_id)
        instance.task_parameters.remove(session.scalars(query).one())
        session.flush()