from fastapi import APIRouter, Depends
from starlette import status
from app.core.auth import RoleChecker
from app.models import UserModel, TaskParametersModel
from app.schemas.task_parameters_schema import TaskParametersOutSchema, TaskParametersOutListSchema, TaskParametersSchema
from app.core.database.db import data_base
from app.utils import object_as_dict, clean_update_data
from sqlalchemy import delete, select, update

task_parameters_router = APIRouter(prefix='/task-parameters', tags=['Task Parameters'])
allow_crd_resource = RoleChecker([UserModel.RoleEnum.admin])
allow_update_resource = RoleChecker([UserModel.RoleEnum.admin, UserModel.RoleEnum.agronomist])


@task_parameters_router.post(
    '/create',
    summary='create task parameter',
    status_code=status.HTTP_200_OK,
    response_model=TaskParametersOutSchema,
    dependencies=[Depends(allow_crd_resource)]
)
async def create(data: TaskParametersSchema):
    with data_base.session() as session:
        instance = TaskParametersModel(**data.model_dump())
        session.add_all([instance])
        session.flush()
        return TaskParametersOutSchema(**object_as_dict(instance))


@task_parameters_router.put(
    '/update/{id}',
    summary='update task parameter',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_update_resource)]
)
async def update_task_parameter(id, data: TaskParametersSchema):
    with data_base.session() as session:
        update_data = clean_update_data(data.model_dump())
        if not update_data:
            return

        session.execute(
            update(TaskParametersModel).
            where(TaskParametersModel.id == id).
            values(**update_data)
        )


@task_parameters_router.delete(
    '/{id}',
    summary='delete task parameter',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_crd_resource)]
)
async def delete_task_parameter(id):
    with data_base.session() as session:
        session.execute(delete(TaskParametersModel).where(TaskParametersModel.id == id))


@task_parameters_router.get(
    '/{id}',
    summary='get task parameter',
    status_code=status.HTTP_200_OK,
    response_model=TaskParametersOutSchema,
    dependencies=[Depends(allow_crd_resource)]
)
async def get(id):
    with data_base.session() as session:
        query = select(TaskParametersModel).filter_by(id=id)
        instance = session.scalars(query).one()
        return TaskParametersOutSchema(**object_as_dict(instance))


@task_parameters_router.get(
    '/list/',
    summary='get task parametr list',
    status_code=status.HTTP_200_OK,
    response_model=TaskParametersOutListSchema,
    dependencies=[Depends(allow_crd_resource)]
)
async def list():
    with data_base.session() as session:
        query = select(TaskParametersModel)
        instance = session.scalars(query).all()
        result = [object_as_dict(x) for x in instance]
        return TaskParametersOutListSchema(
            task_parameters=result
        )