from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from starlette import status

from app.business.user import User
from app.core.auth import RoleChecker, auth
from app.models import UserModel, TaskModel, ProfileModel, WorkingToolsModel
from app.schemas import CreateUserSchema, UserSchema, GetListUserSchema, UpdateUserSchema, ProfileSchema
from app.core.database.db import data_base
from app.schemas.task_schema import TaskOutListSchema, TaskOutSchema
from app.schemas.working_tools_schema import WorkingToolsOutListSchema
from app.utils import object_as_dict, clean_update_data
from sqlalchemy import delete, select, update, desc

user_router = APIRouter(prefix='/user', tags=['User'])
allow_cud_resource = RoleChecker([UserModel.RoleEnum.admin])
allow_read_resource = RoleChecker([UserModel.RoleEnum.admin, UserModel.RoleEnum.agronomist])
allow_add_remove_task = RoleChecker([UserModel.RoleEnum.admin, UserModel.RoleEnum.agronomist])
allow_add_remove_working_tools = RoleChecker([UserModel.RoleEnum.admin])


@user_router.post(
    '/create',
    summary='create user',
    status_code=status.HTTP_200_OK,
    response_model=UserSchema,
    dependencies=[Depends(allow_cud_resource)]
)
async def create(data: CreateUserSchema):
    with data_base.session() as session:
        user = await User.create_in_db(data, session)
        return UserSchema(
            id=user.id,
            email=user.email,
            username=user.username,
            role=user.role,
            phone_number=user.phone_number,
            is_active=user.is_active,
            profile=ProfileSchema(**object_as_dict(user.profile))
        )


@user_router.put(
    '/update/{id}',
    summary='update user',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_cud_resource)]
)
async def update_user(id: UUID, data: UpdateUserSchema):
    with data_base.session() as session:
        update_data = clean_update_data(data.model_dump())
        profile = update_data.pop('profile', None)
        if profile:
            update_profile_data = clean_update_data(profile)
            if update_profile_data:
                session.execute(
                    update(ProfileModel).
                    where(ProfileModel.user_id == id).
                    values(**update_profile_data)
                )
        if not update_data:
            return

        session.execute(
            update(UserModel).
            where(UserModel.id == id).
            values(**update_data)
        )


@user_router.delete(
    '/delete/{id}',
    summary='delete user',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_cud_resource)]
)
async def delete_user(id: UUID):
    with data_base.session() as session:
        session.execute(delete(UserModel).where(UserModel.id == id))


@user_router.get(
    '/{id}',
    summary='get user',
    status_code=status.HTTP_200_OK,
    response_model=UserSchema,
    dependencies=[Depends(allow_read_resource)]
)
async def get(id: UUID):
    with data_base.session() as session:
        user = await User.get_by_id(id, session)
        return UserSchema(
            id=user.id,
            email=user.email,
            username=user.username,
            role=user.role,
            phone_number=user.phone_number,
            is_active=user.is_active,
            profile=object_as_dict(user.profile)
        )


@user_router.get(
    '/list/',
    summary='get users',
    status_code=status.HTTP_200_OK,
    response_model=GetListUserSchema,
    dependencies=[Depends(allow_read_resource)]
)
async def get_list():
    with data_base.session() as session:
        users = await User.get_all(session)
        return GetListUserSchema(
            users=[UserSchema(
                id=x.id,
                email=x.email,
                username=x.username,
                phone_number=x.phone_number,
                role=x.role,
                is_active=x.is_active,
                profile=object_as_dict(x.profile)
            ) for x in users]
        )


@user_router.post(
    '/{id}/add-task/{task_id}',
    summary='add task for user',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_add_remove_task)]
)
async def add_working_tools(id, task_id):
    with data_base.session() as session:
        query = select(UserModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(TaskModel).filter_by(id=task_id)
        instance.task.append(session.scalars(query).one())
        session.flush()


@user_router.post(
    '/{id}/remove-task/{task_id}',
    summary='remove task from user',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_add_remove_task)]
)
async def remove_working_tools(id, task_id):
    with data_base.session() as session:
        query = select(UserModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(TaskModel).filter_by(id=task_id)
        instance.task.remove(session.scalars(query).one())
        session.flush()


@user_router.get(
    '/{id}/get-tasks',
    summary='get tasks for user by user_id',
    status_code=status.HTTP_200_OK,
    response_model=TaskOutListSchema,
    dependencies=[Depends(allow_read_resource)]
)
async def get_tasks_list(id):
    with data_base.session() as session:
        query = select(UserModel).filter_by(id=id)
        instance = session.scalars(query).one()
        result = [object_as_dict(x) for x in instance.task]
        return TaskOutListSchema(
            tasks=result
        )


@user_router.get(
    '/get-tasks/',
    summary='get tasks for user',
    status_code=status.HTTP_200_OK,
    response_model=TaskOutListSchema,
)
async def get_tasks_list(user: UserModel = Depends(allow_read_resource)):
    with data_base.session() as session:
        query = select(TaskModel).filter_by(user_id=user.id).order_by(desc(TaskModel.created_at))
        instance = session.scalars(query).all()
        result = [object_as_dict(x) for x in instance]
        return TaskOutListSchema(
            tasks=result
        )


@user_router.get(
    '/get-closest-task/',
    summary='get closest task for user',
    status_code=status.HTTP_200_OK,
    response_model=TaskOutSchema,
)
async def get_task_closest(user: UserModel = Depends(auth)):
    with data_base.session() as session:
        query = select(
            TaskModel
           ).filter_by(user_id=user.id).where(TaskModel.status != TaskModel.StatusEnum.completed
                                              ).order_by(desc(TaskModel.datetime_start))
        instance = session.scalars(query).first()
        if instance:
            return TaskOutSchema(**object_as_dict(instance))
        else:
            return TaskOutSchema()


@user_router.get(
    '/get-working-tools/',
    summary='get working tools for user',
    status_code=status.HTTP_200_OK,
    response_model=WorkingToolsOutListSchema,
)
async def get_working_tools_list(user: UserModel = Depends(allow_read_resource)):
    with data_base.session() as session:
        query = select(WorkingToolsModel).filter_by(user_id=user.id).order_by(desc(WorkingToolsModel.created_at))
        instance = session.scalars(query).all()
        result = [object_as_dict(x) for x in instance]
        return WorkingToolsOutListSchema(
            working_tools=result
        )


@user_router.post(
    '/{id}/add-working-tools/{working_tools_id}',
    summary='add add working tools for user',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_add_remove_working_tools)]
)
async def add_working_tools(id, working_tools_id):
    with data_base.session() as session:
        query = select(UserModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(WorkingToolsModel).filter_by(id=working_tools_id)
        instance.working_tools.append(session.scalars(query).one())
        session.flush()


@user_router.post(
    '/{id}/remove-working-tools/{working_tools_id}',
    summary='remove working-tools from user',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_add_remove_working_tools)]
)
async def remove_working_tools(id, working_tools_id):
    with data_base.session() as session:
        query = select(UserModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(WorkingToolsModel).filter_by(id=working_tools_id)
        instance.working_tools.remove(session.scalars(query).one())
        session.flush()


