from fastapi import APIRouter, Depends
from starlette import status
from app.core.auth import auth, RoleChecker
from app.models import UserModel, WorkAreaModel
from app.core.database.db import data_base
from app.schemas.work_area_schema import WorkAreaSchema, WorkAreaOutSchema, WorkAreaOutListSchema
from app.utils import object_as_dict, clean_update_data
from sqlalchemy import delete, select, update

work_area_router = APIRouter(prefix='/work-area', tags=['Work Area'])
allow_crud_resource = RoleChecker([UserModel.RoleEnum.admin])


@work_area_router.post(
    '/create',
    summary='create work_area',
    status_code=status.HTTP_200_OK,
    response_model=WorkAreaOutSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def create(data: WorkAreaSchema):
    with data_base.session() as session:
        work_area = WorkAreaModel(**data.model_dump())
        session.add_all([work_area])
        session.flush()
        return WorkAreaOutSchema(**object_as_dict(work_area))


@work_area_router.put(
    '/update/{id}',
    summary='create work_area',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_crud_resource)]
)
async def update_work_area(id, data: WorkAreaSchema):
    with data_base.session() as session:
        update_data = clean_update_data(data.model_dump())
        if not update_data:
            return
        session.execute(
            update(WorkAreaModel).
            where(WorkAreaModel.id == id).
            values(**update_data)
        )


@work_area_router.delete(
    '/{id}',
    summary='delete work area',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_crud_resource)]
)
async def delete_work_area(id):
    with data_base.session() as session:
        session.execute(delete(WorkAreaModel).where(WorkAreaModel.id == id))


@work_area_router.get(
    '/{id}',
    summary='get work area',
    status_code=status.HTTP_200_OK,
    response_model=WorkAreaOutSchema,
    dependencies=[Depends(auth)]
)
async def get(id):
    with data_base.session() as session:
        query = select(WorkAreaModel).filter_by(id=id)
        work_area = session.scalars(query).one()
        return WorkAreaOutSchema(**object_as_dict(work_area))


@work_area_router.get(
    '/list/',
    summary='get work area list',
    status_code=status.HTTP_200_OK,
    response_model=WorkAreaOutListSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def list():
    with data_base.session() as session:
        query = select(WorkAreaModel)
        work_areas = session.scalars(query).all()
        result = [object_as_dict(x) for x in work_areas]
        return WorkAreaOutListSchema(
            work_areas=result
        )
