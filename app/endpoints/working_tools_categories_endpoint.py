from fastapi import APIRouter, Depends
from starlette import status
from app.core.auth import RoleChecker
from app.models import UserModel, WorkingToolsCategoriesModel
from app.schemas.working_tools_categories_schema import (
    WorkingToolsCategoryOutSchema,
    WorkingToolsCategoryOutListSchema,
    WorkingToolsCategorySchema
)
from app.core.database.db import data_base
from app.utils import object_as_dict, clean_update_data
from sqlalchemy import delete, select, update

working_tools_categories_router = APIRouter(prefix='/working_tools_category', tags=['Working Tools Category'])
allow_crud_resource = RoleChecker([UserModel.RoleEnum.admin])


@working_tools_categories_router.post(
    '/create',
    summary='create working tool category',
    status_code=status.HTTP_200_OK,
    response_model=WorkingToolsCategoryOutSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def create(data: WorkingToolsCategorySchema):
    with data_base.session() as session:
        instance = WorkingToolsCategoriesModel(**data.model_dump())
        session.add_all([instance])
        session.flush()
        return WorkingToolsCategoryOutSchema(
            id=instance.id,
            name=instance.name,
        )


@working_tools_categories_router.put(
    '/update/{id}',
    summary='create working tool category',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_crud_resource)]
)
async def update_work_area(id, data: WorkingToolsCategorySchema):
    with data_base.session() as session:
        update_data = clean_update_data(data.model_dump())
        if not update_data:
            return

        session.execute(
            update(WorkingToolsCategoriesModel).
            where(WorkingToolsCategoriesModel.id == id).
            values(**update_data)
        )


@working_tools_categories_router.delete(
    '/{id}',
    summary='delete working tool category',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_crud_resource)]
)
async def delete_work_area(id):
    with data_base.session() as session:
        session.execute(delete(WorkingToolsCategoriesModel).where(WorkingToolsCategoriesModel.id == id))


@working_tools_categories_router.get(
    '/{id}',
    summary='get work tool category',
    status_code=status.HTTP_200_OK,
    response_model=WorkingToolsCategoryOutSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def get(id):
    with data_base.session() as session:
        query = select(WorkingToolsCategoriesModel).filter_by(id=id)
        instance = session.scalars(query).one()
        return WorkingToolsCategoryOutSchema(
            id=instance.id,
            name=instance.name,
        )


@working_tools_categories_router.get(
    '/list/',
    summary='get work tool category list',
    status_code=status.HTTP_200_OK,
    response_model=WorkingToolsCategoryOutListSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def list():
    with data_base.session() as session:
        query = select(WorkingToolsCategoriesModel)
        instance = session.scalars(query).all()
        result = [object_as_dict(x) for x in instance]
        return WorkingToolsCategoryOutListSchema(
            working_tools_categories=result
        )