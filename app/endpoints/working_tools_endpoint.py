from fastapi import APIRouter, Depends
from starlette import status
from app.core.auth import auth, RoleChecker
from app.models import UserModel, WorkingToolsModel, WorkingToolsCategoriesModel
from app.schemas.working_tools_schema import WorkingToolsOutSchema, WorkingToolsOutListSchema, WorkingToolsSchema
from app.core.database.db import data_base
from app.utils import object_as_dict, clean_update_data
from sqlalchemy import delete, select, update

working_tools_router = APIRouter(prefix='/working_tools', tags=['Working Tools'])
allow_crud_resource = RoleChecker([UserModel.RoleEnum.admin])
allow_set_remove_category = RoleChecker([UserModel.RoleEnum.admin])


@working_tools_router.post(
    '/create',
    summary='create working tool',
    status_code=status.HTTP_200_OK,
    response_model=WorkingToolsOutSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def create(data: WorkingToolsSchema):
    with data_base.session() as session:
        instance = WorkingToolsModel(**data.model_dump())
        session.add_all([instance])
        session.flush()
        return WorkingToolsOutSchema(
            id=instance.id,
            name=instance.name,
        )


@working_tools_router.put(
    '/update/{id}',
    summary='create work_tool',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_crud_resource)]
)
async def update_work_area(id, data: WorkingToolsSchema):
    with data_base.session() as session:
        update_data = clean_update_data(data.model_dump())
        if not update_data:
            return

        session.execute(
            update(WorkingToolsModel).
            where(WorkingToolsModel.id == id).
            values(**update_data)
        )


@working_tools_router.delete(
    '/{id}',
    summary='delete work tool',
    status_code=status.HTTP_200_OK,
    dependencies=[Depends(allow_crud_resource)]
)
async def delete_work_area(id):
    with data_base.session() as session:
        session.execute(delete(WorkingToolsModel).where(WorkingToolsModel.id == id))


@working_tools_router.get(
    '/{id}',
    summary='get work tool',
    status_code=status.HTTP_200_OK,
    response_model=WorkingToolsOutSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def get(id):
    with data_base.session() as session:
        query = select(WorkingToolsModel).filter_by(id=id)
        instance = session.scalars(query).one()
        return WorkingToolsSchema(
            id=instance.id,
            name=instance.name,
        )


@working_tools_router.get(
    '/list/',
    summary='get work tool list',
    status_code=status.HTTP_200_OK,
    response_model=WorkingToolsOutListSchema,
    dependencies=[Depends(allow_crud_resource)]
)
async def list():
    with data_base.session() as session:
        query = select(WorkingToolsModel)
        instance = session.scalars(query).all()
        result = [object_as_dict(x) for x in instance]
        return WorkingToolsOutListSchema(
            working_tools=result
        )


@working_tools_router.post(
    "/{id}/set-category/{category_id}",
    summary='set category for working tool',
    dependencies=[Depends(allow_set_remove_category)]
)
async def set_working_tools_category(id, category_id):
    with data_base.session() as session:
        query = select(WorkingToolsModel).filter_by(id=id)
        instance = session.scalars(query).one()
        query = select(WorkingToolsCategoriesModel).filter_by(id=category_id)
        instance.category = session.scalars(query).one()
        session.flush()
