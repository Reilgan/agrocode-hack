import sqlalchemy as sa
from app.core import OrmBaseModel
from app.models.mixin import UuidIdMixin, TimestampMixin
from sqlalchemy import Column
from sqlalchemy import Table
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

user_division_table = Table(
    "user_division",
    OrmBaseModel.metadata,
    Column("division_id", ForeignKey("division.id")),
    Column("user_id", ForeignKey("user.id")),
)

work_area_division_table = Table(
    "work_area_division",
    OrmBaseModel.metadata,
    Column("division_id", ForeignKey("division.id")),
    Column("work_area_id", ForeignKey("work_area.id")),
)

working_tools_division_table = Table(
    "working_tools_division",
    OrmBaseModel.metadata,
    Column("division_id", ForeignKey("division.id")),
    Column("working_tools", ForeignKey("working_tools.id")),
)


class DivisionModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'division'

    name = sa.Column(sa.String, nullable=False, unique=True)
    users = relationship('UserModel', secondary=user_division_table)
    work_area = relationship('WorkAreaModel', secondary=work_area_division_table)
    working_tools = relationship('WorkingToolsModel', secondary=working_tools_division_table)

