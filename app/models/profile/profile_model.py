import sqlalchemy as sa
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from app.core import OrmBaseModel
from app.models.mixin import UuidIdMixin, TimestampMixin
from app.models.user import UserModel
from app.core.database.db import data_base


class ProfileModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'profile'

    user_id = sa.Column(UUID(as_uuid=True), sa.ForeignKey('user.id', ondelete='CASCADE'))

    first_name = sa.Column(sa.String)
    middle_name = sa.Column(sa.String)
    last_name = sa.Column(sa.String)
    birthday = sa.Column(sa.Date)
    timezone = sa.Column(sa.String)

    # relations
    user = relationship('UserModel', back_populates='profile', lazy='noload')
    # passport = relationship('passportModel', lazy='noload')

    # avatar = relationship('ProfileAvatarModel', back_populates='profile', uselist=False, cascade='all, delete-orphan',
    #                       passive_deletes=True)

    def load_user(self):
        with data_base.session() as session:
            self.user = (session.scalars(
                sa.select(UserModel)
                .where(UserModel.id == self.user_id)
            )).one_or_none()
