import enum

import sqlalchemy as sa
from sqlalchemy.orm import relationship

from app.core import OrmBaseModel
from app.models.mixin import TimestampMixin, IntIdMixin, UuidIdMixin
from sqlalchemy.dialects.postgresql import UUID


class TaskModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'task'

    class StatusEnum(enum.Enum):
        not_started = "not_started"
        in_progress = "in_progress"
        on_pause = "on_pause"
        completed = 'completed'

    user_id = sa.Column(UUID(as_uuid=True), sa.ForeignKey('user.id', ondelete='SET NULL'))
    owner_id = sa.Column(UUID(as_uuid=True), sa.ForeignKey('user.id', ondelete='SET NULL'))
    work_area_id = sa.Column(UUID(as_uuid=True), sa.ForeignKey('work_area.id', ondelete='SET NULL'))

    name = sa.Column(sa.String, nullable=False, unique=True)
    description = sa.Column(sa.String, nullable=True)
    status = sa.Column(sa.Enum(StatusEnum), default=StatusEnum.not_started)
    datetime_start = sa.Column(sa.DateTime, nullable=True)
    datetime_end = sa.Column(sa.DateTime, nullable=True)

    # relations
    user = relationship('UserModel', uselist=False, foreign_keys="TaskModel.user_id")
    owner = relationship('UserModel', uselist=False, foreign_keys="TaskModel.owner_id")
    work_area = relationship('WorkAreaModel', back_populates='task', uselist=False)
    working_tools = relationship('WorkingToolsModel', back_populates='task', uselist=True)
    task_parameters = relationship('TaskParametersModel', back_populates='task', uselist=True)