import enum

import sqlalchemy as sa
from sqlalchemy.orm import relationship

from app.core import OrmBaseModel
from app.models.mixin import TimestampMixin, IntIdMixin, UuidIdMixin
from sqlalchemy.dialects.postgresql import UUID


class TaskParametersModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'task_parameters'

    name = sa.Column(sa.String, nullable=False)
    value = sa.Column(sa.String, nullable=False)
    units = sa.Column(sa.String, nullable=False)

    task_id = sa.Column(UUID(as_uuid=True), sa.ForeignKey('task.id', ondelete='SET NULL'))
    task = relationship('TaskModel', back_populates='task_parameters', lazy='noload')
