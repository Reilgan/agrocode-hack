import sqlalchemy as sa
from sqlalchemy.orm import relationship

from app.core import OrmBaseModel
from app.models.mixin import UuidIdMixin, TimestampMixin


class WorkAreaModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'work_area'

    name = sa.Column(sa.String, nullable=False, unique=True)
    description = sa.Column(sa.String, nullable=True)

    latitude = sa.Column(sa.String, nullable=True)
    longitude = sa.Column(sa.String, nullable=True)

    area_size = sa.Column(sa.String, nullable=True)
    units = sa.Column(sa.String, nullable=True)

    task = relationship('TaskModel', back_populates='work_area', lazy='noload')