import sqlalchemy as sa
from sqlalchemy.orm import relationship

from app.core import OrmBaseModel
from app.models.mixin import UuidIdMixin, TimestampMixin


class WorkingToolsCategoriesModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'working_tools_categories'

    name = sa.Column(sa.String, nullable=False)

    # relations
    working_tools = relationship('WorkingToolsModel', back_populates='category', uselist=True)