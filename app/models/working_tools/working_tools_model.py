from sqlalchemy.dialects.postgresql import UUID

import sqlalchemy as sa
from sqlalchemy.orm import relationship

from app.core import OrmBaseModel
from app.models.mixin import UuidIdMixin, TimestampMixin


class WorkingToolsModel(UuidIdMixin, TimestampMixin, OrmBaseModel):
    __tablename__ = 'working_tools'
    user_id = sa.Column(UUID(as_uuid=True), sa.ForeignKey('user.id', ondelete='SET NULL'))
    category_id = sa.Column(UUID(as_uuid=True), sa.ForeignKey('working_tools_categories.id', ondelete='SET NULL'))
    task_id = sa.Column(UUID(as_uuid=True), sa.ForeignKey('task.id', ondelete='SET NULL'))
    name = sa.Column(sa.String, nullable=False, unique=True)

    # relations
    user = relationship('UserModel', back_populates='working_tools', lazy='noload')
    task = relationship('TaskModel', back_populates='working_tools', lazy='noload')

    category = relationship('WorkingToolsCategoriesModel', back_populates='working_tools', lazy='noload')