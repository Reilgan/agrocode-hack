from uuid import UUID
from pydantic import BaseModel, Field

from app.schemas import UserSchema
from app.schemas.work_area_schema import WorkAreaSchema
from app.schemas.working_tools_schema import WorkingToolsSchema


class DivisionSchema(BaseModel):
    name: str


class DivisionOutSchema(BaseModel):
    id: UUID
    name: str
    users: list[UserSchema] = None
    work_area: list[WorkAreaSchema] = None
    working_tools: list[WorkingToolsSchema] = None


class DivisionOutListSchema(BaseModel):
    divisions: list[DivisionOutSchema]
