from starlette import status

from app.core import ValidationErrorMixin, ValidationError


@ValidationError.register
class SchemasException(ValidationErrorMixin, Exception):
    pass


class RegistrationFieldException(SchemasException):
    code = 'registration'
    message = "Password: must be longer than 8 characters and consist of latin characters, Phone number: +71111111111"

    def __init__(self, **kwargs):
        super().__init__(code=self.code, message=self.message, **kwargs)


