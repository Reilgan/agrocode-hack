from uuid import UUID
from pydantic import BaseModel


class TaskParametersSchema(BaseModel):
    name: str
    value: str
    units: str


class TaskParametersOutSchema(TaskParametersSchema):
    id: UUID
    task_id: UUID | None = None


class TaskParametersOutListSchema(BaseModel):
    task_parameters: list[TaskParametersOutSchema]