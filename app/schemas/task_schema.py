from datetime import datetime
from uuid import UUID

from pydantic import BaseModel

from app.models import TaskModel


class TaskSchema(BaseModel):
    name: str | None = None
    description: str | None = None
    status: TaskModel.StatusEnum | None = None
    datetime_start: datetime | None = None
    datetime_end: datetime | None = None


class TaskOutSchema(TaskSchema):
    id: UUID | None = None
    user_id: UUID | None = None
    work_area_id: UUID | None = None
    owner_id: UUID | None = None
    working_tools: list[UUID] | None = None
    task_parameters: list[UUID] | None = None
    created_at: datetime | None = None
    updated_at: datetime | None = None


class TaskOutListSchema(BaseModel):
    tasks: list[TaskOutSchema]