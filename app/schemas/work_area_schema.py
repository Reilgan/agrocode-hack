from uuid import UUID
from pydantic import BaseModel


class WorkAreaSchema(BaseModel):
    name: str | None = None
    description: str | None = None
    latitude: str | None = None
    longitude: str | None = None
    area_size: str | None = None
    units: str | None = None


class WorkAreaOutSchema(WorkAreaSchema):
    id: UUID


class WorkAreaOutListSchema(BaseModel):
    work_areas: list[WorkAreaOutSchema]