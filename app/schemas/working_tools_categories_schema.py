from uuid import UUID
from pydantic import BaseModel


class WorkingToolsCategorySchema(BaseModel):
    name: str


class WorkingToolsCategoryOutSchema(WorkingToolsCategorySchema):
    id: UUID


class WorkingToolsCategoryOutListSchema(BaseModel):
    working_tools_categories: list[WorkingToolsCategoryOutSchema]