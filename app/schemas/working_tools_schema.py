from uuid import UUID
from pydantic import BaseModel


class WorkingToolsSchema(BaseModel):
    name: str
    user_id: UUID | None
    category_id: UUID | None


class WorkingToolsOutSchema(WorkingToolsSchema):
    id: UUID


class WorkingToolsOutListSchema(BaseModel):
    working_tools: list[WorkingToolsOutSchema]