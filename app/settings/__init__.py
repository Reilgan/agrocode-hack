import os

DEPLOY_ENV = os.getenv('DEPLOY_ENV', 'local')

# JWT-Token
JWT_ALGORITHM = os.getenv('JWT_ALGORITHM', "HS256")
JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY', '')
JWT_REFRESH_SECRET_KEY = os.getenv('JWT_REFRESH_SECRET_KEY', '')
ACCESS_TOKEN_EXPIRE_MINUTES = os.getenv('ACCESS_TOKEN_EXPIRE_MINUTES', 60 * 24 * 7)  # 30 minutes
REFRESH_TOKEN_EXPIRE_MINUTES = os.getenv('REFRESH_TOKEN_EXPIRE_MINUTES', 60 * 24 * 7)  # 7 days

# Database
DATABASE_URL = os.getenv('DATABASE_URL')

# middleware
ALLOW_CORS = os.getenv('ALLOW_CORS', None)

# cache
CACHE_URL = os.getenv('CACHE_URL', None)


def debug_mode() -> bool:
    return DEPLOY_ENV == 'local'
