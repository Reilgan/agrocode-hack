from app.utils.singleton import Singleton


class DataManager(metaclass=Singleton):
    _unregistered_user_data: dict = {
        'email': 'test@email.com',
        'bad_email': 'qwerty',
        'password': '12345678Qwerty',
        'bad_password': '1',
        'phone_number': '+79999999999',
        'bad_phone_number': '89234qweqw1234',
        'profile': {

        }
    }

    _registered_user_data: dict = {
        'email': 'reg@email.com',
        'password': 'regpassword',
        'phone_number': '+78888888888',
        'profile': {

        }
    }

    @property
    def unregistered_user_data(self) -> dict:
        return self._unregistered_user_data

    @property
    def registered_user_data(self) -> dict:
        return self._registered_user_data
