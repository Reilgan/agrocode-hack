import pytest
from httpx import AsyncClient

from app.tests.data import DataManager


async def send_email_code(client: AsyncClient, data_manager: DataManager) -> str:
    response = await client.post(
        '/registration/send-email-code',
        json={
            'email': data_manager.unregistered_user_data.get('email'),
            'phone_number': data_manager.unregistered_user_data.get('phone_number'),
            'password': data_manager.unregistered_user_data.get('password')
        }
    )

    assert response.status_code == 200
    return response.json()['key']


async def enter_email_code(client: AsyncClient, key: str, data_manager: DataManager):
    response = await client.post(
        '/registration/enter-email-code',
        json={
            'key': key,
            'verification_code': 0
        }
    )
    assert response.status_code == 200
    assert response.json()['email'] == data_manager.unregistered_user_data.get('email')


@pytest.mark.asyncio
async def test_registration_endpoint(client: AsyncClient, data_manager: DataManager):
    hash_key = await send_email_code(client, data_manager)
    await enter_email_code(client, hash_key, data_manager)




