
def dict_to_hash(dict_: dict) -> str:
    return str(hash(frozenset(dict_)))


def str_to_hash(value: str) -> str:
    return str(hash(value))

