from secrets import choice


def digit4_verification_code():
    return ''.join((str(choice(range(10))) for _ in range(4)))


def digit6_verification_code():
    return ''.join((str(choice(range(10))) for _ in range(6)))
