#!/bin/bash

until check-db-connection | grep -q "^success"; do
    >&2 echo "Postgres is still unavailable"
    sleep 1
done

alembic upgrade head
#load-data
uvicorn --host 0.0.0.0 --port 8000 --reload app.asgi:app